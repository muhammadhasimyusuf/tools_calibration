import React from 'react'
import Each from './Each'
import { menu } from './MenuModel'
import { Link, useLocation } from 'react-router-dom'

function Menu() {

    const location = useLocation()
    const path = location.pathname
    console.log({menu})
  return (
<div className="flex justify-center w-full bg-white  bg-opacity-50 backdrop-blur">
    <div className='flex gap-3'>

        <Each
            of={menu}
            render={data =>  <div className={`w-36 ${path === data.link && 'shadow-inner bg-gray-100'} hover:bg-transparent rounded-lg drop-shadow-sm cursor-pointer hover:shadow-lg hover:shadow-emerald-200 border-white border-t-2 border-l-2 hover:rounded`}>
                <Link key={data.link} to={data.link} className='flex justify-center w-full gap-3'>
                 {data.icon &&   <img className='w-5 h-5 self-center' src={data.icon}/>}
                    <h3 className="text-center text-black  col-end-4 w-12">{data.label}</h3>
                </Link>
                </div>}
        />
    
    </div>

</div>
  )
}

export default Menu