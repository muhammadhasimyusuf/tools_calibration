import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const questions = [
  {
    question: "Apa fungsi utama dari avometer analog?",
    options: [
      "Mengukur tegangan, arus, dan resistansi",
      "Mengukur suhu",
      "Mengukur kecepatan",
      "Mengukur tekanan",
    ],
    answer: "Mengukur tegangan, arus, dan resistansi",
  },
  {
    question:
      "Bagian dari avometer analog yang digunakan untuk mengubah pengukuran dari tegangan ke arus disebut?",
    options: ["Probe", "Saklar selektor", "Jarum penunjuk", "Skala pengukur"],
    answer: "Saklar selektor",
  },
  {
    question: "Apa yang ditunjukkan oleh jarum penunjuk pada avometer analog?",
    options: [
      "Kondisi baterai",
      "Nilai pengukuran",
      "Jenis pengukuran",
      "Kalibrasi alat",
    ],
    answer: "Nilai pengukuran",
  },
  {
    question:
      "Mengapa penting untuk memilih skala yang tepat pada avometer analog sebelum melakukan pengukuran?",
    options: [
      "Untuk menghemat baterai",
      "Untuk menghindari kerusakan alat",
      "Untuk mendapatkan hasil yang lebih akurat",
      "Untuk mempercepat pengukuran",
    ],
    answer: "Untuk mendapatkan hasil yang lebih akurat",
  },
  {
    question:
      "Apa yang harus dilakukan jika jarum penunjuk tidak bergerak saat melakukan pengukuran?",
    options: [
      "Memeriksa koneksi probe",
      "Mengganti baterai",
      "Mengganti avometer",
      "Mengukur ulang pada skala yang lebih tinggi",
    ],
    answer: "Memeriksa koneksi probe",
  },
  {
    question:
      "Saat mengukur resistansi, posisi saklar selektor harus berada di posisi mana?",
    options: ["ACV", "DCV", "Ω", "mA"],
    answer: "Ω",
  },
  {
    question: "Apa yang ditunjukkan oleh simbol Ω pada avometer analog?",
    options: ["Tegangan AC", "Tegangan DC", "Arus", "Resistansi"],
    answer: "Resistansi",
  },
  {
    question:
      "Bagaimana cara mengukur tegangan AC menggunakan avometer analog?",
    options: [
      "Mengatur saklar selektor ke ACV",
      "Menghubungkan probe ke sumber tegangan AC",
      "Mengatur saklar selektor ke DCV",
      "a dan b benar",
    ],
    answer: "a dan b benar",
  },
  {
    question: "Apa fungsi dari skala cermin pada avometer analog?",
    options: [
      "Mengukur sudut",
      "Mengurangi kesalahan paralaks",
      "Memperbaiki kalibrasi",
      "Menyimpan data pengukuran",
    ],
    answer: "Mengurangi kesalahan paralaks",
  },
  {
    question:
      "Mengapa penting untuk menekan tombol reset pada avometer analog sebelum mengukur resistansi?",
    options: [
      "Untuk menghemat baterai",
      "Untuk mendapatkan hasil yang akurat",
      "Untuk mengukur tegangan",
      "Untuk mengkalibrasi ulang alat",
    ],
    answer: "Untuk mendapatkan hasil yang akurat",
  },
  {
    question:
      "Apa yang terjadi jika Anda mengukur tegangan tinggi dengan skala yang terlalu rendah?",
    options: [
      "Pengukuran menjadi lebih akurat",
      "Jarum penunjuk tidak bergerak",
      "Avometer dapat rusak",
      "Tidak ada yang terjadi",
    ],
    answer: "Avometer dapat rusak",
  },
  {
    question:
      "Bagaimana cara membaca nilai pengukuran yang ditunjukkan oleh jarum penunjuk?",
    options: [
      "Mengabaikan skala",
      "Melihat skala yang sesuai dengan pengaturan saklar selektor",
      "Mengira-ngira nilainya",
      "Melihat skala tertinggi",
    ],
    answer: "Melihat skala yang sesuai dengan pengaturan saklar selektor",
  },
  {
    question:
      "Apa yang harus dilakukan jika jarum penunjuk menunjukkan nilai negatif saat mengukur tegangan DC?",
    options: [
      "Membalikkan posisi probe",
      "Mengganti baterai",
      "Mengubah skala",
      "Mengukur ulang",
    ],
    answer: "Membalikkan posisi probe",
  },
  {
    question:
      "Pada posisi apa saklar selektor harus ditempatkan untuk mengukur arus DC?",
    options: ["ACV", "DCV", "mA atau A", "Ω"],
    answer: "mA atau A",
  },
  {
    question:
      "Mengapa avometer analog tidak boleh digunakan untuk mengukur tegangan lebih tinggi dari kapasitasnya?",
    options: [
      "Untuk menghemat baterai",
      "Untuk menghindari kerusakan alat",
      "Untuk mendapatkan hasil yang akurat",
      "Untuk mengurangi kesalahan pengukuran",
    ],
    answer: "Untuk menghindari kerusakan alat",
  },
  {
    question: "Apa yang ditunjukkan oleh skala mA pada avometer analog?",
    options: ["Tegangan", "Arus dalam miliampere", "Resistansi", "Tegangan AC"],
    answer: "Arus dalam miliampere",
  },
  {
    question:
      "Apa yang harus dilakukan jika hasil pengukuran resistansi tidak stabil?",
    options: [
      "Mengganti baterai",
      "Mengkalibrasi alat",
      "Mengubah skala",
      "Mengukur ulang dengan avometer lain",
    ],
    answer: "Mengganti baterai",
  },
  {
    question:
      "Bagaimana cara menghindari kesalahan paralaks saat membaca avometer analog?",
    options: [
      "Menggunakan avometer digital",
      "Melihat jarum penunjuk sejajar dengan cermin",
      "Mengatur skala tertinggi",
      "Mengukur ulang beberapa kali",
    ],
    answer: "Melihat jarum penunjuk sejajar dengan cermin",
  },
  {
    question:
      "Apa yang harus diperhatikan saat mengukur tegangan AC dengan avometer analog?",
    options: [
      "Mengatur saklar selektor ke DCV",
      "Menggunakan probe dengan benar",
      "Mengukur resistansi terlebih dahulu",
      "Menghubungkan ke arus DC",
    ],
    answer: "Menggunakan probe dengan benar",
  },
  {
    question:
      "Mengapa penting untuk memeriksa kondisi baterai avometer analog secara berkala?",
    options: [
      "Untuk menghemat baterai",
      "Untuk mendapatkan hasil yang akurat",
      "Untuk mengukur arus dengan tepat",
      "Untuk memperpanjang umur alat",
    ],
    answer: "Untuk mendapatkan hasil yang akurat",
  },
  {
    question: "Apa fungsi utama dari osiloskop analog?",
    options: [
      "Mengukur resistansi",
      "Mengukur frekuensi",
      "Menampilkan bentuk gelombang sinyal listrik",
      "Mengukur suhu",
    ],
    answer: "Menampilkan bentuk gelombang sinyal listrik",
  },
  {
    question:
      "Bagian manakah dari osiloskop analog yang digunakan untuk mengatur sensitivitas vertikal?",
    options: ["Knob waktu/div", "Knob volt/div", "Knob trigger", "Probe"],
    answer: "Knob volt/div",
  },
  {
    question:
      "Apa yang ditampilkan pada sumbu horizontal (X) osiloskop analog?",
    options: ["Tegangan", "Waktu", "Frekuensi", "Arus"],
    answer: "Waktu",
  },
  {
    question: "Apa yang ditampilkan pada sumbu vertikal (Y) osiloskop analog?",
    options: ["Waktu", "Tegangan", "Frekuensi", "Arus"],
    answer: "Tegangan",
  },
  {
    question: "Untuk apa knob trigger digunakan pada osiloskop analog?",
    options: [
      "Mengatur intensitas cahaya",
      "Menstabilkan tampilan gelombang",
      "Mengubah skala waktu",
      "Mengukur resistansi",
    ],
    answer: "Menstabilkan tampilan gelombang",
  },
  {
    question: 'Apa yang dimaksud dengan "time base" pada osiloskop analog?',
    options: [
      "Pengaturan tegangan",
      "Pengaturan waktu per divisi",
      "Pengaturan frekuensi",
      "Pengaturan arus",
    ],
    answer: "Pengaturan waktu per divisi",
  },
  {
    question:
      "Mengapa penting untuk mengkalibrasi osiloskop sebelum digunakan?",
    options: [
      "Untuk menghemat energi",
      "Untuk mendapatkan hasil pengukuran yang akurat",
      "Untuk meningkatkan kontras layar",
      "Untuk memperpanjang umur alat",
    ],
    answer: "Untuk mendapatkan hasil pengukuran yang akurat",
  },
  {
    question:
      "Bagaimana cara mengukur frekuensi sinyal menggunakan osiloskop analog?",
    options: [
      "Menghitung jumlah divisi pada sumbu Y",
      "Mengatur knob volt/div",
      "Mengukur periode sinyal pada sumbu X dan menghitung kebalikannya",
      "Menggunakan probe khusus",
    ],
    answer: "Mengukur periode sinyal pada sumbu X dan menghitung kebalikannya",
  },
  {
    question:
      "Apa yang harus dilakukan jika sinyal pada layar osiloskop terlalu kecil untuk diamati dengan jelas?",
    options: [
      "Meningkatkan skala volt/div",
      "Mengurangi skala waktu/div",
      "Mengatur knob trigger",
      "Menggunakan osiloskop digital",
    ],
    answer: "Meningkatkan skala volt/div",
  },
  {
    question:
      'Apa yang ditunjukkan oleh fungsi "AC/DC coupling" pada osiloskop analog?',
    options: [
      "Pengaturan untuk memilih antara sinyal AC atau DC",
      "Pengaturan intensitas layar",
      "Pengaturan skala waktu",
      "Pengaturan jenis probe yang digunakan",
    ],
    answer: "Pengaturan untuk memilih antara sinyal AC atau DC",
  },
];
function Quiz() {
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [score, setScore] = useState(0);
  const [showScore, setShowScore] = useState(false);

  const handleAnswerOptionClick = (isCorrect) => {
    if (isCorrect) {
      setScore(score + 1);
    }

    const nextQuestion = currentQuestion + 1;
    if (nextQuestion < questions.length) {
      setCurrentQuestion(nextQuestion);
    } else {
      setShowScore(true);
    }
  };

  return (
   <>
      <style jsx>{`
        .app {
          font-family: Arial, sans-serif;
          text-align: center;
          background-color: #f5f5f5;
          padding: 20px;
          border-radius: 10px;
          width: 60%;
          margin: auto;
          box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
        }

        .question-section {
          margin-bottom: 20px;
        }

        .question-count {
          margin-bottom: 10px;
          font-weight: bold;
        }

        .question-text {
          margin-bottom: 20px;
        }

        .answer-section button {
          background-color: #007bff;
          color: white;
          border: none;
          border-radius: 5px;
          padding: 10px 20px;
          margin: 5px;
          cursor: pointer;
          font-size: 16px;
        }

        .answer-section button:hover {
          background-color: #0056b3;
        }

        .score-section {
          font-size: 24px;
          font-weight: bold;
          color: #333;
        }
      `}</style>
     <div className="app">
      {showScore ? (
        <div className="score-section">
          <h1 className="text-5xl font-extrabold text-center mb-3 text-transparent bg-clip-text bg-gradient-to-r from-black to-purple-600 ">Score : {Math.round((score / questions.length) * 100)}</h1>
          {/* <br /> */}
          <p>Anda menjawab benar {score} dari {questions.length} soal</p>
        </div>
      ) : (
        <>
          <div className="question-section">
            <div className="question-count">
              <span>Pertanyaan {currentQuestion + 1}</span>/{questions.length}
            </div>
            <div className="question-text">
              {questions[currentQuestion].question}
            </div>
          </div>
          <div className="answer-section">
            {questions[currentQuestion].options.map((option) => (
              <button
                onClick={() =>
                  handleAnswerOptionClick(
                    option === questions[currentQuestion].answer
                  )
                }
              >
                {option}
              </button>
            ))}
          </div>
        </>
      )}
    </div>
   </>
  );
}

export default Quiz;
