import { PiBook } from 'react-icons/pi'
import multimeter from '../assets/icon/multimeter.png'
import oscilloscope from '../assets/icon/oscilloscope.png'
import alat from '../assets/Img/alat.jpg'
import materi from '../assets/Img/materi.jpg'
import { SlPuzzle } from 'react-icons/sl'
import { BiHome } from 'react-icons/bi' 

export const menu = [
    {
        label : 'Osiloskop',
        link : '/oscilloscope',
        // link : '/',
        icon : oscilloscope
    },
    {
        label : 'Avo',
        link : '/avometer',
        icon : multimeter
    },
    {
        label : 'Home',
        link : '/',
        // icon : ''
    }
]

export const menuMain = [
    {
        label : 'Praktik Alat Ukur',
        link : '/oscilloscope',
        // link : '/',
        img : alat
    },
    {
        label : 'Materi Pembelajaran',
        link : '/',
        img : materi
    }
]

