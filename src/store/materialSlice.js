import { produce } from "immer";

const materialSlice = (set) => ({
   state : {
    materials : [],
    index : 0
   },
    
   actions : {
    setMaterials: (data) => set(produce((state) => {
        state.state.materials = data;
      })),
    setIndex: (data) => set(produce((state) => {
            state.state.index = data
      })),
   }
   
});




export default materialSlice