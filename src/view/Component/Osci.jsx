import React, { useState } from "react";
import osiloscope from "../../assets/Img/osiloscope.png";
import { Button, Popover } from 'antd';
import { motion } from 'framer-motion';
import { ImInfo } from "react-icons/im";
import ReactPlayer from "react-player/youtube";
import { CgClose } from "react-icons/cg";

const style = {
  // backgroundImage: `url(${osiloscope})`,
  backgroundRepeat: "no-repeat",
  backgroundPosition: 'center',
  backgroundSize: 'contain'
};

function Osci() {
  const [showCalibration, setShowCalibration] = useState(false);

  return (
    <motion.div
      id="Osci" style={style}
      className="relative flex top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 justify-center h-[65vh] bg-transparent backdrop-blur rounded shadow-inner drop-shadow-lg w-[70%]"
    >
      {
        showCalibration ? (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ duration: 1 }}
            exit={{ opacity: 0, duration: 1 }}
            className="w-full h-full bg-black"
          >
            <Button
              className="fixed bg-red-500 top-0 right-0 z-40"
              size="small"
              onClick={() => setShowCalibration(false)}
            >
              <CgClose />
            </Button>
            <ReactPlayer playing={showCalibration} url={'https://youtu.be/jACxJauLRwA'} width="100%" height="100%" controls={true} />
          </motion.div>
        )
          :
          (
            <>
              <div className="absolute">
                <Button
                  className=" z-40"
                  onClick={() => setShowCalibration(true)}
                  icon={<ImInfo />}
                >
                  Cara Kalibrasi
                </Button>
              </div>
              <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                transition={{ duration: 1 }}
                className="relative right-0 top-0 w-full h-full border"
              >
                <img className="absolute w-[70%] h-[90%] left-[13%]" src={osiloscope} alt="" />
                <Popover placement="top" content={<div>Ch 1 Volt / Div</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[7%] w-[4%] absolute" style={{ top: '30%', left: '48%' }}></div>
                </Popover>
                <Popover placement="right" content={<div>Probe Ch2</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[4%] w-[2%] absolute" style={{ top: '49%', left: '56%' }}></div>
                </Popover>
                <Popover placement="left" content={<div>Probe Ch1</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[4%] w-[2%] absolute" style={{ top: '49%', left: '49%' }}></div>
                </Popover>
                <Popover placement="top" content={<div>Power</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[4%] w-[2%] absolute" style={{ top: '48%', left: '43%' }}></div>
                </Popover>
                <Popover placement="right" content={<div>Mode Selector</div>}>
                  <div className="rounded-full bg-black opacity-10 hover:cursor-pointer h-[4%] w-[2%] absolute" style={{ top: '22%', left: '53%' }}></div>
                </Popover>
                <Popover placement="left" content={<div>Vertical Control</div>}>
                  <div className="rounded-full hover:bg-black opacity-15 hover:cursor-pointer h-[4%] w-[10%] absolute" style={{ top: '19.3%', left: '49%' }}></div>
                </Popover>
                <Popover placement="right" content={<div>Horizontal Control</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[4%] w-[2%] absolute" style={{ top: '19%', left: '64.5%' }}></div>
                </Popover>
                <Popover placement="left" content={<div>Triger Control</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[4%] w-[7%] absolute" style={{ top: '19%', left: '70%' }}></div>
                </Popover>
                <Popover placement="bottom" content={<div>Time / Div</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[7%] w-[4%] absolute" style={{ top: '30%', left: '63.3%' }}></div>
                </Popover>
                <Popover placement="top" content={<div>Ch 2 Volt / Div</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[7%] w-[4%] absolute" style={{ top: '30%', left: '56%' }}></div>
                </Popover>
                <Popover placement="top" content={<div>Kontrol Intensitas</div>}>
                  <div className="rounded-full bg-black opacity-10 hover:cursor-pointer h-[4%] w-[2%] absolute" style={{ top: '50.3%', left: '23.4%' }}></div>
                </Popover>
                <Popover placement="bottom" content={<div>Kontrol Fokus</div>}>
                  <div className="rounded-full hover:bg-black opacity-10 hover:cursor-pointer h-[4%] w-[2%] absolute" style={{ top: '50.3%', left: '29%' }}></div>
                </Popover>
                <Popover placement="top" content={<div>Monitor</div>}>
                  <div className="rounded hover:bg-black opacity-15 hover:cursor-pointer h-[27%] w-[19.5%] absolute" style={{ top: '15.3%', left: '23%' }}></div>
                </Popover>
              </motion.div>
            </>
          )
      }
    </motion.div>
  );
}

export default Osci;
