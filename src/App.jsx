import { useState } from "react";
import "./App.css";
import { AnimatePresence } from "framer-motion";
import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import HomePage from "./view/Home/HomePage";
import Cointainer from "./view/Main/Cointainer";
import Osci from "./view/Component/Osci";
import Avo from "./view/Component/Avo";
import Quiz from "./view/Quiz/Quiz";

function App() {
  const location = useLocation();
  return (
    <Cointainer>
      <AnimatePresence>
        <Routes location={location} key={location.pathname}>
          <Route exact path="/" element={<HomePage/>} />
          <Route exact path="/oscilloscope" element={<Osci />} />
          <Route exact path="/avometer" element={<Avo />} />
          <Route exact path="/quiz" element={<Quiz />} />
        </Routes>
      </AnimatePresence>
    </Cointainer>
  );
}

export default App;
