import React, { useState } from "react";
import BACKGROUND from '../../assets/Img/BACKGROUND.jpg'
import { Layout, Button, theme } from "antd";
import {
  AiFillTool,
  AiOutlineMenuFold,
  AiOutlineMenuUnfold,
  AiOutlineUser,
} from "react-icons/ai";
import Menu from "../../components/Menu";
import { useLocation } from "react-router-dom";
import logo1 from "../../assets/Img/logo1.jpg";
import logo2 from "../../assets/Img/logo2.jpg";
import logo3 from "../../assets/Img/logo3.jpg";
import logo4 from "../../assets/Img/loogo4.jpg";
const { Header, Sider, Content } = Layout;

function Cointainer({ children }) {
  const [collapsed, setCollapsed] = useState(false);

  const location = useLocation();
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <Layout className="">
      {/* <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="demo-logo-vertical" />
        <Menu
         
          theme="dark"
          mode="inline"
          items={[
            {
              key: '1',
              icon: <AiFillTool />,
              label: 'Alat',
            },
            // {
            //   key: '2',
            //   icon: <VideoCameraOutlined />,
            //   label: 'nav 2',
            // },
            // {
            //   key: '3',
            //   icon: <UploadOutlined />,
            //   label: 'nav 3',
            // },
          ]}
        />
      </Sider> */}
      <Layout className="max-h-screen h-screen">
        <Header
          className="w-full flex"
          style={{ padding: 0, background: colorBgContainer }}
        >
          <div className="flex gap-5">
            <img className="w-32 h-auto self-center" src={logo1} alt="" />
            <img className="w-24 h-auto self-center" src={logo2} alt="" />
          </div>
          <div className="flex justify-center w-[70%]">
          {location.pathname != "/" && <Menu />}
          </div>
          <div className="flex justify-end gap-5">
            <img className="w-16 h-auto self-center rounded-full" src={logo4} alt="" />
            <img className="w-32 h-auto self-center" src={logo3} alt="" />
          </div>
          {/* <Button
            type="text"
            icon={collapsed ? <AiOutlineMenuUnfold  /> : <AiOutlineMenuFold />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: '16px',
              width: 64,
              height: 64,
            }}
          /> */}
        </Header>
        <Content
          className="bg-black"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            // background: colorBgContainer,
            borderRadius: borderRadiusLG,
            backgroundImage: `url(${BACKGROUND})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
}

export default Cointainer;
