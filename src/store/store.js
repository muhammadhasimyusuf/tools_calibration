import { create } from "zustand";
import materialSlice from "./materialSlice";

export const useBoundStore = create((set) => ({
    ...materialSlice(set),
}))