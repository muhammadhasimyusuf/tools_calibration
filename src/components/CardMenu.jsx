import React, { useState } from 'react'
import Each from './Each';
import { NavLink } from 'react-router-dom';
import { menu, menuMain } from './MenuModel';
import { useBoundStore } from '../store/store';
import Potensio from '../assets/Img/potensio.png';
import analog from '../assets/Img/analog.png';
import transistor from '../assets/Img/transistor.png';
import { Image } from 'antd';

function CardMenu({setModalOpen}) {
const materi = [
  {
    title : "Componen Pasif",
    content : [
      <div className='!text-white p-7'>
        <p>Komponen pasif adalah komponen yang tidak memerlukan sumber daya eksternal untuk beroperasi dan tidak dapat memperkuat sinyal elektronik.
           Komponen ini hanya mempengaruhi sinyal elektronik melalui penyimpanan atau pembebasan energi, perubahan impedansi, atau pembagian tegangan.
           Berikut adalah beberapa contoh dan penjelasan mengenai komponen pasif yang umum digunakan:</p>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Resistor</dt>
          <dd className='ml-5'>
            <img className='w-1/3 rounded' src="https://akcdn.detik.net.id/community/media/visual/2023/11/23/resistor_169.jpeg?w=700&q=90" alt="" />
          </dd>
          <dd className='ml-5'>
            <b>Pengertian dan Fungsi:</b> 
            Resistor adalah komponen yang menahan aliran arus listrik, mengurangi tegangan, dan membatasi jumlah arus yang mengalir dalam rangkaian. 
            Resistor memiliki nilai resistansi yang diukur dalam ohm (Ω).
          </dd>
          <dd className='ml-5'>
              <b>Jenis-jenis Resistor:</b>
               Ada resistor tetap, variabel (potensiometer), dan thermistor.
               Resistor tetap memiliki nilai resistansi yang tidak berubah, sementara potensiometer dapat diatur nilainya. 
               Thermistor memiliki resistansi yang berubah sesuai dengan suhu.
          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Kapasitor</dt>
          <dd className='ml-5'>
            <img className='w-1/3 rounded' src="https://lirp.cdn-website.com/6ce3488c/dms3rep/multi/opt/kapasitor+adalah+komponen+elektronika-1920w.png" alt="" />
          </dd>
          <dd className='ml-5'>
            <b>Pengertian dan Fungsi:</b> 
            Kapasitor adalah komponen yang menyimpan energi dalam bentuk medan listrik antara dua pelat konduktor yang dipisahkan oleh bahan dielektrik. 
            Kapasitor digunakan untuk menyimpan dan melepaskan muatan listrik, memblokir arus DC sambil memungkinkan arus AC mengalir, dan dalam penyaringan sinyal.
          </dd>
          <dd className='ml-5'>
              <b>Jenis-jenis Resistor:</b>
              Ada kapasitor elektrolit, keramik, tantalum, dan film. 
              Kapasitor elektrolit memiliki kapasitas tinggi dan polaritas, sedangkan kapasitor keramik tidak memiliki polaritas dan sering digunakan dalam aplikasi frekuensi tinggi.
          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Induktor</dt>
          <dd className='ml-5'>
            <img className='w-1/3 rounded' src="http://m.id.ht-transformers.com/uploads/201611419/p201611111016476499790.jpg?size=390x0" alt="" />
          </dd>
          <dd className='ml-5'>
            <b>Pengertian dan Fungsi:</b> 
            Induktor adalah komponen yang menyimpan energi dalam bentuk medan magnet ketika arus listrik mengalir melaluinya. 
            Induktor digunakan untuk menyimpan energi, menyaring sinyal, dan dalam rangkaian resonansi.
          </dd>
          <dd className='ml-5'>
              <b>Jenis-jenis Resistor:</b>
              Induktor bisa berupa induktor tetap dan variabel, serta toroidal dan choke. 
              Induktor toroidal memiliki bentuk melingkar dan efisiensi tinggi, sedangkan choke digunakan untuk menghambat arus AC atau frekuensi tinggi.
          </dd>
          
        </dl>
      </div>
    ],
  },
  {
    title : "Komponen Aktif",
    content : [
      <div className='!text-white p-7'>
        <p>Komponen aktif adalah komponen yang memerlukan sumber daya eksternal untuk beroperasi dan dapat mengontrol aliran arus listrik.
           Komponen ini mampu memperkuat sinyal elektronik dan digunakan dalam berbagai aplikasi seperti penguat, osilator, dan sakelar elektronik. 
          Berikut adalah beberapa contoh dan penjelasan mengenai komponen aktif yang umum digunakan:</p>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Transistor</dt>
          <dd className='ml-5'>
            <img className='w-1/3 rounded' src="https://circuitspedia.com/wp-content/uploads/2018/06/Transistor.jpg" alt="" />
          </dd>
          <dd className='ml-5'>
            <b>Jenis - jenis transistor:</b> 
            BJT memiliki tiga terminal yaitu emitor, basis, dan kolektor. Tipe BJT yang umum adalah NPN dan PNP. BJT digunakan untuk memperkuat arus.
          </dd>
          <dd className='ml-5'>
              <b>BJT (Bipolar Junction Transistor):</b>
               Ada resistor tetap, variabel (potensiometer), dan thermistor.
               Resistor tetap memiliki nilai resistansi yang tidak berubah, sementara potensiometer dapat diatur nilainya. 
               Thermistor memiliki resistansi yang berubah sesuai dengan suhu.
          </dd>
          <dd className='ml-5'>
              <b>FET (Field Effect Transistor):</b>
              FET memiliki tiga terminal yaitu gate, source, dan drain. 
              Jenis FET yang umum adalah JFET (Junction FET) dan MOSFET (Metal-Oxide-Semiconductor FET). 
              FET digunakan untuk memperkuat tegangan dan arus dengan kontrol yang lebih baik dan efisiensi tinggi.
          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Dioda</dt>
          <dd className='ml-5'>
            <img className='w-1/3 rounded' src="https://blog.indobot.co.id/wp-content/uploads/2023/07/Dioda.jpg" alt="" />
          </dd>
          <dd className='ml-5'>
            <b>Jenis Dioda:</b> 
            Dioda adalah komponen yang memungkinkan arus mengalir hanya dalam satu arah. Ada berbagai jenis dioda seperti dioda penyearah, dioda zener, dan LED (Light Emitting Diode).
          </dd>
          <dd className='ml-5'>
              <b>Dioda Penyearah:</b>
              Dioda ini digunakan untuk mengubah arus AC menjadi arus DC.
          </dd>
          <dd className='ml-5'>
              <b>Dioda Zener:</b>
              Dioda zener digunakan untuk menyediakan tegangan referensi yang stabil.
          </dd>
          <dd className='ml-5'>
              <b>LED (Light Emitting Diode):</b>
              LED digunakan sebagai sumber cahaya dalam berbagai aplikasi mulai dari indikator kecil hingga pencahayaan umum.
          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>IC (Integrated Circuit)</dt>
          <dd className='ml-5'>
            <img className='w-1/3 rounded' src="https://i0.wp.com/semiengineering.com/wp-content/uploads/Ansys_What-is-an-Integrated-Circuit-fig2.webp?w=400&ssl=1" alt="" />
          </dd>
          <dd className='ml-5'>
            <b>Pengertian dan Fungsi:</b> 
            Induktor adalah komponen yang menyimpan energi dalam bentuk medan magnet ketika arus listrik mengalir melaluinya. 
            Induktor digunakan untuk menyimpan energi, menyaring sinyal, dan dalam rangkaian resonansi.
          </dd>
          <dd className='ml-5'>
              <b>Pengertian IC:</b>
              IC adalah rangkaian elektronik yang terintegrasi dalam satu chip kecil yang dapat mengandung ribuan komponen seperti transistor, dioda, resistor, dan kapasitor.
          </dd>
          <dd className='ml-5'>
              <b>Jenis-jenis IC:</b>
              IC bisa berupa IC analog (seperti op-amp), IC digital (seperti mikroprosesor dan memori), dan IC campuran (mixed-signal IC).
          </dd>
          
        </dl>
      </div>
    ],
  },
  {
    title : "OSILOSKOP",
    content : [
      <div className='!text-white p-7'>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Definisi</dt>
          <dd className='ml-5'>
          Osiloskop adalah alat ukur elektronika yang fungsinya memproyeksikan bentuk sinyal listrik agar dapat dilihat dan dipelajari. Pada osiloskop dilengkapi dengan tabung sinar katode. Kemudian peranti pemancar elektron akan memproyeksikan sorotan elektron ke layar tabung sinar katode. Sorotan elektron tersebut membekas pada layar. Rangkaian khusus dalam osiloskop akan menyebabkan sorotan bergerak berulang-ulang dari kiri ke kanan. Proses pengulangan ini menyebabkan bentuk sinyal yang berkelanjutan sehingga dapat dipelajari. Osiloskop dapat digunakan untuk merekam sinyal tegangan dari waktu ke waktu. Penganalisisan logika akan merekam hingga 16 sinyal logika independen untuk sinyal digital. Serangkaian komponen masukan dan keluaran logika yang disederhanakan tersebut dapat mempermudah penyidikan rangkaian digital.
          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Kegunaan</dt>
          <dd className='ml-5'>
          Osiloskop digunakan untuk mengamati bentuk gelombang yang tepat dari sinyal listrik. Osiloskop selain dapat menunjukkan amplitudo sinyal, dapat juga menunjukkan distorsi, waktu antara dua peristiwa (seperti lebar pulsa, periode, atau waktu naik) dan waktu relatif dari dua sinyal yang saling berkaitan. Semua alat ukur elektronik bekerja berdasarkan sampel data, yang mana jika semakin tinggi sampel data, maka semakin akurat peralatan elektronik tersebut. Osiloskop juga mempunyai sampel data yang sangat tinggi, oleh sebab itu osiloskop merupakan alat ukur elektronik yang mahal. Apabila sebuah osiloskop mempunyai sampel rate 10 Ks/s (10 kilo sample/second = 10.000 data per detik), maka osiloskop akan melakukan pembacaan sebanyak 10.000 kali dalam sedetik. Apabila yang diukur adalah sebuah gelombang dengan frekuensi 2500 Hz, maka setiap sampelnya akan memuat data 1/4 dari sebuah gelombang penuh yang selanjutnya akan ditampilkan dalam layar monitor dengan grafik skala XY. Osiloskop juga dapat digunakan untuk melihat osilasi arus bolak-balik. Pada osiloskop, kita dapat melihat gambar tegangan yang berosilasi hingga frekuensi beberapa MegaHertz. Pada osiloskop, pola yang tampil pada layar akan tampak diam. Dari gambar tersebut maka kita dapat menentukan frekuensi tegangan maupun amplitudonya. Osiloskop dapat menampilkan pendeteksian gelombang pada sebuah mikrofon, yang dihasilkan dari dua pengeras suara untuk dapat ditampilkan di osiloskop sehingga bentuk gelombangnya tampil di layar. 
          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Fungsi</dt>
          <dd className='ml-5'>
          Osiloskop adalah alat ukur elektronika yang berfungsi memproyeksikan bentuk sinyal listrik sehingga dapat dilihat dan dipelajari. Alat ukur osiloskop dilengkapi dengan tabung sinar katode. Kemudian peranti pemancar elektron akan memproyeksikan sorotan elektron ke layar tabung sinar katode. Sorotan elektron tersebut membekas pada layar. Alat ukur osiloskop ini memiliki komponen utama yang disebut tabung katode dan terpasang dalam rangkaian. Rangkaian osiloskop yang digunakan tersebut akan menyorot sehingga terjadi gerakan yang berulang-ulang pada layar monitor dari kiri ke kanan. Hal tersebut kemudian menyebabkan bentuk sinyal terjadi secara berkelanjutan sehingga terlihat seperti gelombang. Untuk mengetahui rambut yang retak, rongga, dan retak struktur pada beton, maka peralatan ini dapat dipadukan dengan osiloskop yang kemudian ditampilkan pada layar monitor. Penganalisis spektrum pada peralatan osiloskop dan perekam data gelombang dalam bentuk digital terdiri dari satu unit akuisisi yang dihubungkan ke komputer. Perpindahan gerak partikel gelombang R yang diterima sensor merupakan data analog akselerasi yang diubah ke dalam bentuk digital melalui unit akuisisi dan selanjutnya berjalan operasi spektrum yang dilakukan oleh komputer. 
Fungsi dari alat ukur osiloskop ini yaitu:  <br />
•	Dapat menyelidiki peristiwa atau gejala yang bersifat periodik <br />
•	Dapat mengamati bentuk gelombang kotak dari tegangan listrik <br />
•	Dapat menganalisis gelombang dalam rangkaian elektronika <br />
•	Dapat mengamati amplitudo tegangan, frekuensi, periode dari sinyal yang tidak diketahui <br />
•	Dapat mengetahui amplitudo modulasi yang dihasilkan pemancar radio maupun generator pembangkit sinyal <br />
•	Dapat menganalisis karakteristik besaran yang berubah-ubah terhadap waktu yang terlihat pada layar monitor <br />
•	Dapat mengetahui beda fasa antara sinyal masukan dan sinyal keluaran <br />
•	Dapat mengukur tegangan arus searah atau arus bolak-balik dan menghitung frekuensi.

          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Cara kerja</dt>
          <dd className='ml-5'>
          
          Osiloskop bekerja dengan cara tabung sinar katode yang memiliki prinsip kerja sebagai elektron yang kemudian dipancarkan dari katode. Kemudian akan menumbuk bidang gambar yang dilapisi zat yang bersifat flourecent. Bidang gambar pada proses tersebut berfungsi sebagai anode. Arah pergerakan elektron pada osiloskop dipengaruhi oleh medan listrik dan medan magnetik. Pada osiloskop, sinar katode mengandung medan gaya listrik yang digunakan untuk menggerakan elektron ke arah anode. Lempeng kapasitor yang terpasang vertikal ini akan menghasilkan medan listrik, yang kemudian menumbuk garis lurus vertikal dinding gambar. Apabila lempeng horizontal dipasang pada tegangan periodik, maka elektron yang pada awalnya bergerak secara vertikal kemudian akan bergerak secara horizontal dengan kecepatan yang tetap sehingga gelombang yang muncul pada layar monitor akan membentuk grafik sinusoidal. Osiloskop juga dapat digunakan untuk mengukur tegangan riak yang terdapat dalam tegangan arus searah dengan mengatur posisi kopling arus bolak-balik pada osiloskop. Kapasitor yang terdapat pada terminal masukan tersebut dipakai untuk melewatkan tegangan arus bolak-balik dan menahan tegangan arus searah. Hasil yang lebih tepat dapat diperoleh ketika osiloskop digunakan untuk menghasilkan dan mengukur sinyal. 

          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Cara penggunaan</dt>
          <dd className='ml-5'>
          
          Langkah-langkah dalam menggunakan osiloskop, sebagai berikut: <br /> 
•	Langkah pertama yang perlu dilakukan adalah mengkalibrasi dengan cara menghubungkan osiloskop pada jaringan listrik kemudian menghidupkannya. Selanjutnya mengamati tampilan pada layar monitor yang akan muncul garis lurus mendatar (apabila tidak ada sinyal yang masuk). <br />
•	Selanjutnya atur fokus, intensitas, kemiringan, X posisi, Y posisi, dengan mengatur posisi tersebut kemudian mengamati hasil pengukuran. <br />
•	Langkah selanjutnya gunakan tegangan referensi pada osiloskop sehingga dapat melakukakan kalibrasi. Ada dua tegangan referensi yang dapat dijadikan acuan yaitu tegangan persegi 2 vpp dan 0,2 vpp dengan frekuensi 1 KHz. <br />
•	Masukan probe pada terminal acuan sehingga akan muncul tegangan persegi yang kemudian akan tertera pada layar monitor. <br />
•	Apabila yang dijadikan acuan adalah tegangan 2 vpp, maka pada posisi 1 Volt/div (satu kotak vertikal mewakili tegangan 1 Volt) akan terdapat nilai tegangan dari puncak ke puncak sebayak 2 kotak dan untuk time/div 1 ms/div (satu kotak horizontal mewakili waktu 1 ms) harus terdapat satu gelombang untuk satu kotak.



          </dd>
          
        </dl>
     
      </div>
    ],
  },
  {
    title : "AVOMETER",
    content : [
      <div className='!text-white p-7'>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Definisi</dt>
          <dd className='ml-5'>
          Avometer merupakan alat yang digunakan untuk mengukur hambatan, tegangan dan arus listrik.
Fungsi avometer ini sangat banyak dan dimanfaatkan oleh teknisi untuk melakukan tiga jenis pengukuran listrik dengan satu alat. Berikut ini adalah ulasan lengkap mengenai avometer.

          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Fungsi</dt>
          <dd className='ml-5'>
          Fungsi dari avometer ini sangat kompleks untuk melakukan pengukuran tegangan listrik pada beberapa aspek. Avometer ini banyak dimanfaatkan sebagai alat ukur yang digunakan pada bidang kelistrikan otomotif dan elektronika lainnya.
Berdasarkan namanya, avometer merupakan kombinasi alat ukur berupa ampere, volt, dan ohm meter. Berikut ini adalah ulasan mengenai fungsi dari avometer atau multimeter berdasarkan kegunaan dari ketiga jenis alat ukur yang ada di dalamnya. <br />
•	Volt Meter <br />
Fungsi voltmeter yang ada pada avometer digunakan untuk mengukur tegangan listrik dalam rangkaian dengan satuan volt. Klasifikasi tegangan listrik yang bisa diukur menggunakan avometer ini adalah mulai dari tekanan sangat rendah hingga rendah.
Anda bisa mengukur tegangan listrik menggunakan avometer dengan menghubungkan kedua kabel dengan tekanan berbeda. Pada saat menggunakan alat ini Anda harus mengetahui batas ukur tegangan agar tidak mudah rusak. <br />
•	Ohm Meter <br />
Ohm meter yang ada pada avometer ini mempunyai kegunaan utama untuk mengukur besarnya tahanan atau hambatan. Di samping itu, ohm meter pada alat ini berguna untuk mencari tahu apakah komponen pada suatu rangkaian dalam kondisi terhubung atau tidak.
Pemasangan avometer untuk mengukur tekanan ini adalah dengan menghubungkan kedua kabel probe ke ujung resistor. Anda bisa menjaga ketahanan avometer ini dengan cara memperhatikan rangkaian yang akan diukur agar alat ini bisa bertahan lama. <br />
•	Ampere Meter <br />
Satu lagi fungsi dari avometer terutama pada bagian amperemeter pada alat ini adalah untuk mengukur besaran arus listrik. Anda bisa mengukur aliran arus listrik pada rangkaian tertutup dengan satuan mili ampere menggunakan avometer tersebut.
Arus listrik yang bisa diukur menggunakan avometer ini relatif kecil dan secara spesifik untuk DC. <br />
Anda bisa menghubungkan avometer dengan memasang seri kabel probe pada suatu rangkaian. Sebelum memasang kabel probe ini Anda harus memutus rangkaian terlebih dahulu.





          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Bagian Bagian Avometer</dt>
          <dd className='ml-5'>
          
Ada beberapa bagian penting di dalam alat ukur kelistrikan tersebut untuk memaksimalkan kinerjanya. 
Setiap bagian dari avometer dibekali dengan fungsi masing-masing yang saling berkaitan. Berikut ini adalah penjelasan mengenai bagian-bagian avometer yang perlu Anda ketahui. <br />
•	Mirror/Cermin <br />
Fungsi cermin dalam avometer ini adalah melakukan pengukuran berdasarkan petunjuk dari jarum meter. Anda harus memposisikan pandangan tegak lurus dengan avometer agar bisa mendapatkan hasil yang tepat sesuai dengan tanda tanpa adanya bayangan di cermin. <br />
•	Zero Connection <br />
Zero connection merupakan bagian yang mempunyai fungsi untuk mengenolkan jarum pada posisi kiri. Alat ini akan bekerja pada saat avometer mulai mengukur arus dan tegangan listrik pada suatu rangkaian. <br />
•	Batas Ukur <br />
Avometer juga mempunyai bagian untuk membatasi nilai maksimal yang bisa diukur. Batas ukur ini juga dilengkapi dengan beberapa petunjuk yang dibedakan menjadi beberapa blok. Beberapa petunjuk tersebut berguna untuk mengukur AC, DC, dan resistansi. <br />
•	Measuring Terminal <br />
Bagian selanjutnya pada multimeter adalah measuring terminal sebagai konektor dengan rangkaian yang akan ditiru. Measuring terminal ini terdiri dua kutub positif dengan warna merah dan negatif yang ditandai dengan warna hitam. <br />
•	Scale (Skala Maksimum) <br />
Scale adalah bagian yang berguna untuk menampilkan batas nilai tertinggi pada panel pengukuran. Batas skala maksimum untuk mengukur resistansi nilainya bisa dilihat dari arah kanan ke kiri. Sementara untuk mengukur tegangan, DC, dan AC dapat dilihat dari kiri ke kanan. <br />
•	Pointer (Jarum Meter) <br />
Fungsi jarum pada avometer adalah sebagai petunjuk dalam pengukuran, sehingga Anda bisa langsung membaca hasilnya. <br />
•	Ohm Adjustment <br />
Ohm meter adalah bagian dari avometer yang berguna untuk mengenolkan jarum pada posisi kanan saat proses pengukuran hambatan. <br />
•	Range Selector <br />
Satu lagi bagian dari alat ini adalah range sektor yang mempunyai fungsi untuk memilih batasan tegangan, arus, dan hambatan. <br />
          </dd>
          
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Cara Kerja Avometer</dt>
          <dd className='ml-5'>
          
Cara kerja avometer ini berdasarkan pada fungsinya untuk mengukur tegangan, hambatan, dan arus listrik. Berikut ini adalah ulasan mengenai petunjuk cara kerja dalam menggunakan avometer. <br />
•	Mengukur Tegangan AC <br />
Langkah pertama yang harus dilakukan untuk mengukur tegangan AC adalah mengatur posisi selektor ke ACV. Apabila Anda menggunakan multimeter analog, maka harus memilih nilai tegangan pada selector di posisi 300 volt.
Hal berikutnya adalah menghubungkan probe ke terminal yang akan diukur tanpa membingungkan posisi positif dan negatifnya. Selanjutnya tahan probe tersebut agar Anda bisa mendapatkan hasil yang ditunjukkan dan mulai membacanya. <br />
•	Mengukur Hambatan <br />
AvoMeter dalam pengukuran hambatan ini bekerja untuk mengetahui apakah kabel yang sedang digunakan ini dalam kondisi putus atau tidak. Alat ini biasanya digunakan untuk melakukan hambatan pada kelistrikan body.
Anda harus menentukan perkiraan nilai hambatan terlebih dahulu dan mengawalinya dengan tanda x pada avometer analog. Lalu, hubungkan probe ke resistor dan tahan keduanya hingga mendapatkan hasil yang di display pada layar. <br />
•	Mengukur Tekanan DC <br />
Cara kerja avometer selanjutnya adalah untuk mengukur tegangan DC, salah satunya adalah pada mobil. Hal penting yang harus diperhatikan dalam pengukuran tekanan DC adalah pada saat menghubungkan probe merah ke terminal positif dan hitam ke negatif. <br />
•	Mengukur Arus Listrik <br />
Satu lagi cara kerja avometer yang perlu Anda ketahui, yaitu untuk mengukur arus listrik untuk perbaikan kelistrikan pada bodi mobil. Anda bisa menggunakan soket apabila ingin mendapatkan hasil pengukuran yang tepat.
Fungsi avometer untuk mengukur hambatan, tegangan, dan arus listrik mempunyai banyak manfaat untuk beberapa bidang pekerjaan. Anda bisa mempelajari cara kerja avometer terlebih dahulu agar bisa menggunakan alat ini secara maksimal.

          </dd>
          
        </dl>
      
      </div>
    ],
  },


  {
    title: "Langkah-langkah Pengukuran Menggunakan Avometer",
    content: [
      <div className='!text-white p-7'>
        <dl>
          <dt className='ml-5 text-bold text-xl'>LED</dt>
          <dd className='ml-5'>
            <b>Menguji apakah LED masih dalam kondisi baik:</b>
            <ol className='list-decimal ml-5'>
              <li>Menentukan kutub positif (kaki panjang) dan negatif (kaki pendek) dari LED.</li>
              <li>Arahkan selector pada skala ohm.</li>
              <li>Letakkan probe pada kaki kaki LED. Untuk probe merah (+) diletakkan pada kaki pendek (katoda) dan probe hitam (-) diletakkan di kaki panjang (anoda).</li>
              <li>Apabila LED menyala, maka LED berada dalam kondisi baik.</li>
            </ol>
          </dd>
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Potensiometer</dt>
          <dd className='ml-5'>
            <b>Mengukur nilai resistansi potensiometer:</b>
            <ol className='list-decimal ml-5'>
              <li>Arahkan saklar selektor pada posisi ohm meter. Jika yang diukur adalah 50K, maka kita bisa gunakan x 1K atau x 10K.</li>
              <li>Tempelkan probe merah (pencolok merah) ke ujung kaki potensio begitu juga dengan yang hitam.</li>
             <div className='w-1/2'>
             <Image
                src={Potensio}
              />
             </div>
              <li>Perhatikan jarum multimeter, jika jarum bergerak dan menghasilkan nilai 50K (sesuai ukuran hambatan maksimum potensio) berarti nilai kedua kaki bagian ujung potensio masih bagus, sedangkan jika menunjuk nilai selain itu berarti hambatan kedua kaki potensio tersebut sudah rusak.</li>
              <li>Tempelkan pencolok merah ke ujung kaki potensio dan yang satunya di kaki tengah pada potensio.</li>
              <li>Putar potensio sambil melihat jarum multimeter, jika jarum bergerak teratur sesuai dengan putaran dan nilai maksimum 50K, berarti potensio masih bagus.</li>
            </ol>
          </dd>
          <dd className='ml-5'>
            <b>Tambahan:</b> Cara mengukur potensiometer dengan multimeter – Kita dapat mengukur nilai resistansi dari sebuah potensiometer dengan menggunakan alat ukur yang dinamakan multimeter, baik multimeter yang menunjukkan nilai digital maupun multimeter analog. Seperti yang kita ketahui bahwa multimeter adalah alat ukur yang terdiri dari gabungan pengukuran arus listrik (ampere), tegangan listrik (volt), dan resistansi/hambatan (ohm). Untuk mengukur potensiometer yang merupakan komponen keluarga resistor, potensiometer tentunya diukur dengan fungsi ohm (resistansi) yang terdapat pada multimeter. Dalam pengukuran, kita dapat mengetahui nilai maksimum resistansi sebuah potensiometer dan juga perubahan nilai resistansi potensiometer saat kita memutar shaft atau tuas pengaturnya.
          </dd>
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Kapasitor</dt>
          <dd className='ml-5'>
            <b>Cara mengukur kapasitor dengan multimeter analog:</b>
            <ol className='list-decimal ml-5'>
              <li>Aturlah posisi skala selektor ke ohm dengan skala x1K.</li>
              <li>Hubungkan probe merah ke kaki kapasitor positif.</li>
              <li>Hubungkan probe hitam ke kaki kapasitor negatif.</li>
              <li>Periksa jarum yang terdapat pada tampilan display. Jika kondisi kapasitor baik, jarum bergerak naik dan kemudian kembali lagi. Sedangkan, jika rusak, jarum bergerak naik tetapi tidak kembali lagi atau juga jarum tidak naik sama sekali.</li>
            </ol>
            <p>Untuk mengukurnya sebaiknya gunakan multimeter atau ohm meter analog atau biasa yang orang hafal adalah avometer jangan yang digital, karena untuk menentukan elco masih bagus atau rusak dengan melihat gerak pada jarum analog.</p>
            <Image
              src={analog}
            />
          </dd>
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Elco</dt>
          <dd className='ml-5'>
            <b>Cara mengukur elco dengan mudah:</b>
            <ol className='list-decimal ml-5'>
              <li>Pastikan saklar selector multimeter berada pada ohm meter dengan ketentuan:</li>
              <li>Jika elco berukuran kecil maka arahkan selector pada pengali ohm yang besar, namun jika elco berukuran besar arahkan selector pada pengali yang kecil. Misalnya elco 1uF/10V maka arahkan selector pada x 10K ohm namun jika elco ukuran 6800uF/60V arahkan saklar selector pada x 1 atau x 10 ohm.</li>
              <li>Setelah itu tempelkan kedua ujung multimeter pada masing-masing kaki elco sambil memperhatikan jarum multimeter.</li>
              <li>Jika jarum bergerak menuju skala tertentu kemudian kembali lagi menuju skala 0 (mentok ke kiri) berarti elco masih dalam keadaan baik atau bagus.</li>
              <li>Namun bila jarum bergerak menuju skala tertentu dan kembali tidak sampai ke 0 (mentok ke kiri) berarti elco dalam keadaan setengah baik/setengah rusak.</li>
              <li>Jika elco bergerak sampai mentok dan tidak kembali, berarti elco sudah konslet (terjadi hubung singkat).</li>
              <li>Jika elco tidak bergerak sama sekali berarti elco sudah putus dan tidak bisa digunakan lagi.</li>
            </ol>
          </dd>
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Dioda</dt>
          <dd className='ml-5'>
            <b>Cara mengukur dioda dengan multimeter analog:</b>
            <ol className='list-decimal ml-5'>
              <li>Atur alihkan posisi ke posisi ohm (Ω).</li>
              <li>Hubungkan probe hitam ke terminal katoda (tanda cincin).</li>
              <li>Hubungkan probe merah ke terminal anoda. Baca hasil pengukuran dengan multimeter tampilan.</li>
              <li>Layar harus menunjukkan nilai tertentu (misalnya 0,64 MΩ).</li>
              <li>Pindahkan probe hitam ke terminal anoda dan probe merah ke katoda.</li>
              <li>Baca hasil pengukuran dengan multimeter tampilan.</li>
              <li>Nilai resistansinya adalah tak terhingga (infinity) atau rangkaian terbuka. Dioda mungkin rusak sebelum nilai tertentu.</li>
            </ol>
          </dd>
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Transistor</dt>
          <dd className='ml-5'>
            <b>Mengukur transistor dengan multimeter analog:</b>
            <div className='w-1/2'>
                <Image
                      src={transistor}
                />
            </div>
            <ol className='list-decimal ml-5'>
              <li><b>Cara mengukur transistor PNP dengan multimeter analog:</b></li>
              <ol className='list-decimal ml-5'>
                <li>Atur posisi saklar pada posisi ohm (Ω) x1k atau x10k.</li>
                <li>Hubungkan probe merah pada terminal basis (B) dan probe hitam pada terminal emitor (E). Jika jarum bergerak ke kanan menunjukan nilai tertentu, berarti transistor tersebut dalam kondisi baik.</li>
                <li>Pindahkan probe hitam pada terminal kolektor (C), jika jarum bergerak ke kanan menunjukan nilai tertentu, berarti transistor tersebut dalam kondisi baik.</li>
              </ol>
              <li><b>Cara mengukur transistor NPN dengan multimeter analog:</b></li>
              <ol className='list-decimal ml-5'>
                <li>Atur posisi saklar pada posisi ohm (Ω) x1k atau x10k.</li>
                <li>Hubungkan probe hitam pada terminal basis (B) dan probe merah pada terminal emitor (E). Jika jarum bergerak ke kanan menunjukan nilai tertentu, berarti transistor tersebut dalam kondisi baik.</li>
                <li>Pindahkan probe merah pada terminal kolektor (C), jika jarum bergerak ke kanan menunjukan nilai tertentu, berarti transistor tersebut dalam kondisi baik.</li>
              </ol>
              <p>Catatan: Jika tata letak probe dibalikan dari cara yang disebutkan diatas, maka jarum pada multimeter analog harus tidak akan bergerak sama sekali atau “open”.</p>
            </ol>
          </dd>
        </dl>
        <dl>
          <dt className='ml-5 text-bold text-xl'>Resistor</dt>
          <dd className='ml-5'>
            <b>Langkah-langkah:</b>
            <ol className='list-decimal ml-5'>
              <li>Melakukan kalibrasi yaitu dengan meletakkan selector pada pilihan ohm dan pertemukan kedua ujung probe sampai pointer menunjuk angka nol agar pembacaan nantinya akurat.</li>
              <li>Melakukan pengukuran terhadap resistor dengan mempertemukan kedua ujung probe dengan kedua kaki resistor.</li>
              <li>Membaca nilai hambatan yang ditunjukkan oleh jarum penunjuk pada skala yang dipilih.</li>
            </ol>
          </dd>
        </dl>
      </div>
    ]
  }
  
]


    const [showFor, setshowFor] = useState('');
    const {setMaterials, setIndex} = useBoundStore(state => state.actions)
  return (
    <div className=" rounded shadow-2xl bg-white bg-opacity-70 backdrop-blur mx-auto p-4 flex items-center justify-center">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-4 bg-white opacity-100">
               
    <Each
        of={menuMain}
        render={
            (item, index) => {
                return (
                    <div onClick={e => {
                      if(item.label === "Materi Pembelajaran"){
                        e.stopPropagation()
                        e.preventDefault()
                        setMaterials(materi)
                        setModalOpen(true)
                      }
                    }} onMouseLeave={e => setshowFor('')} onMouseOver={e => setshowFor(item.label)} id={item.label} className="max-w-sm mx-auto relative shadow-md rounded-lg cursor-pointer">
                    <img src={item.img} alt="Img by Meriç Dağlı https://unsplash.com/@meric" className="w-full h-full object-cover rounded-lg" />
                 <NavLink to={item.label !== 'Teori' &&   item.link}>
                    <div className={`${showFor !== item.label && 'hidden'} absolute bottom-0 left-0 right-0 h-full  bg-black bg-opacity-50 backdrop-blur text-white p-4 rounded-b-lg`}>
                      <h1 className="text-2xl font-semibold text-center pt-44">{item.label}</h1>
                      {/* <p className="mt-2">This is a beautiful nature image placeholder. You can replace it with your own image.</p> */}
                    </div>
                    </NavLink>
                  </div>
                )
            }}
    />
            </div>
        </div>
  )
}

export default CardMenu