import React, { useState } from "react";
import Avometer from "../../assets/Img/Avo.png";
import { Button, Drawer, Popover } from "antd";
import { motion } from "framer-motion";
import Each from "../../components/Each";
import ReactPlayer from "react-player/youtube";

const style = {
  // backgroundImage: `url(${Avometer})`,
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  backgroundSize: 'contain',
};

const listTut = [
  {
    label: "LED",
    url : "https://youtu.be/8sOREMTs6ho",
    position : "right"
  },
  {
    label: "Potensio",
    url : 'https://youtu.be/Mx1d9IvjjXk',
    position : "top"
  },
  {
    label: "Transistor",
    url: 'https://youtu.be/BkZj_xWPp-k',
    position : 'top'
  },
  {
    label: "Dioda",
    url: 'https://youtu.be/9tOkUzZLGfk',
    position : 'top'
  },
  {
    label: "Kapasitor",
    url: 'https://youtu.be/nQeSd79Dl0E',
    position : 'left'
  },
  {
    label: "Resistor",
    url: 'https://youtu.be/bqcs_Bw71Eg',
    position : 'bottom'
  },
];

function Avo() {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [ledDrawerOpen, setledDrawerOpen] = useState(false);
  const [showFor, setshowFor] = useState({});
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.2 }}
      exit={{ opacity: 0 }}
      id="avometer"
      style={style}
      className="relative flex top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 bg-transparent backdrop-blur opacity-50 justify-center h-[65vh] rounded shadow-inner drop-shadow-lg w-[70%]"
    >
      <Button
        htmlType="button"
        type="dashed"
        onClick={() => setDrawerOpen(true)}
        className="absolute top-0 right-0 bg-white z-50"
      >
        Cara Pengukuran
      </Button>
      <Drawer
        title="Cara Pengukuran"
        placement="left"
        closable
        onClose={() => setDrawerOpen(false)}
        open={drawerOpen}
        getContainer={false}
      >
        <Each
          of={listTut}
          render={(data) => (
            <div
              onClick={(e) => {
                setshowFor(data);
                setledDrawerOpen(true);
              }}
              className="py-3 border-b cursor-pointer rounded hover:bg-gray-50"
            >
              {data.label}
            </div>
          )}
        />
      </Drawer>
      <div className="relative w-full h-full border">
      <img className="w-1/2 h-full  left-[25%] top-0  absolute " src={Avometer} alt="" />
        <Popover placement="right" content={<div>Zero Adjust Knob</div>}>
          <div className="rounded-full hover:bg-violet-500 opacity-50 hover:cursor-pointer h-[5%] w-[5%] absolute" style={{ top: '52%', left: '54%' }}></div>
        </Popover>
        <Popover placement="top" content={<div>Probe (+)</div>}>
          <div className="hover:bg-violet-500 opacity-20 hover:cursor-pointer h-[40%] w-[2%] absolute" style={{ top: '30%', left: '60%' }}></div>
        </Popover>
        <Popover placement="top" content={<div>Probe (-)</div>}>
          <div className="hover:bg-violet-500 opacity-20 hover:cursor-pointer h-[40%] w-[2%] absolute" style={{ top: '30%', left: '63%' }}></div>
        </Popover>
        <Popover placement="top" content={<div>Selector</div>}>
          <div className="hover:bg-violet-500 opacity-30 hover:cursor-pointer h-[17%] w-[2%] absolute  rotate-[-9deg]" style={{ top: '60%', right: '55.5%' }}></div>
        </Popover>
        <Popover placement="leftTop" content={<div>Jarum Penunjuk</div>}>
          <div className="hover:bg-red-500 opacity-20 hover:cursor-pointer h-[20%] w-[1%] absolute transform -rotate-[53deg]" style={{ top: '24%', left: '37.5%' }}></div>
        </Popover>
      </div>

      <Drawer
        title={showFor.label}
        placement={showFor.position}
        size="large"
        closable
        onClose={() => setledDrawerOpen(false)}
        open={ledDrawerOpen}
      >
        <div className="w-full h-full">
          <ReactPlayer playing={ledDrawerOpen} key={showFor.label} url={showFor.url} width="100%" height="100%" controls={true} />
        </div>
      </Drawer>
    </motion.div>
  );
}

export default Avo;
