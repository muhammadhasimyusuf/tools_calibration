import React, { useState } from "react";
import { motion } from "framer-motion";
import CardMenu from "../../components/CardMenu";
import { Modal } from "antd";
import Materials from "../../components/Materials";

function HomePage() {
  const [materialsModalOpen, setmaterialsModalOpen] = useState(false);
  return (
    <>
      <p 
      style={{
        fontFamily:"Lucida Handwriting"
      }}
      className="text-purple-200 text-5xl text-center " > Selamat Datang </p>
    <motion.div
      className=" flex justify-center w-full drop-shadow-2xl absolute top-52"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.2 }}
      exit={{ opacity: 0 }}
    >
      {/* <div> */}
<div>
      <CardMenu
        setModalOpen={setmaterialsModalOpen}
      />
        <p 
        style={{
          fontFamily:"Lucida Handwriting"
        }}
        className="text-pink-200 text-center text-5xl mt-10">
          E-Modul Praktik Alat Ukur 
      </p>
</div>
      {/* </div> */}
    </motion.div>


    <Modal
        title={[<div className="w-full   border-b border-black">Materi</div>]}
        centered
        open={materialsModalOpen}
        onOk={() => setmaterialsModalOpen(false)}
        onCancel={() => setmaterialsModalOpen(false)}
        className="!w-full h-screen max-w-7xl"
        footer={null}
      >
        
            <Materials/>
      </Modal>
    
    </>
  );
}

export default HomePage;
