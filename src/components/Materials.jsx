import React, { useEffect, useState } from "react";
import { useBoundStore } from "../store/store";
import { Button } from "antd";
import { BiLeftArrow, BiRightArrow } from "react-icons/bi";
import {motion} from 'framer-motion'
import { MySwal } from "./Swal";
import { useNavigate } from "react-router-dom";

function Materials() {

  const materials = useBoundStore(state => state.state.materials)
  const [index, setIndex] =  useState(0);

  const [prevIndex, setPrevIndex] = useState(index);
  const [direction, setDirection] = useState('left-to-right');

  useEffect(() => {
    if (index > prevIndex) {
      setDirection('right-to-left');
    } else if (index < prevIndex) {
      setDirection('left-to-right');
    }
    setPrevIndex(index);
  }, [index]);

  const variants = {
    enter: (direction) => ({
      x: direction === 'left-to-right' ? window.innerWidth : -window.innerWidth,
      width: 0,
    }),
    center: {
      x: 0,
      width: '100%',
    },
    exit: (direction) => ({
      x: direction === 'left-to-right' ? -window.innerWidth : window.innerWidth,
      transition: { duration: 0.1 },
    }),
  };
  const navigate = useNavigate();

  const handlePage = e => {
    const type = e.target.id
    console.log({type})
    if(type === "next"){
      const isLastPage = index === materials?.length - 1
      console.log({isLastPage})
      if(isLastPage){
        MySwal.fire({
          icon: 'question',
          title: 'Go to Quiz?',
          showCancelButton: true,
        }).then((result) => {
          if (result.isConfirmed) {
           navigate('/quiz')
          }
        })
      }
       index != materials?.length - 1 && setIndex(index + 1)
    }
    if(type === "prev"){
      index > 0 && setIndex(index - 1)
    }
  }


  return (
    <motion.div
    // key={index}
    // initial={{ width: 0 }}
    // animate={{ width: '100%' }}
    // exit={{ x: window.innerWidth, transition : {duration : 0.1} }}
      key={index}
      custom={direction}
      initial="enter"
      animate="center"
      exit="exit"
      variants={variants}
      className="max-h-[831px] overflow-y-auto overflow-x-hidden"
      id="materials"
    >
      <div className="text-left flex flex-col w-full items-center justify-center  md:borde dark:border-slate-600 rounded-xl">
        <div className="w-full">
          <div
            href="/"
            rel="noreferrer"
            target="_blank"
            className="bg-white dark:bg-slate-800 rounded-xl w-full flex flex-col md:flex-row justify-start shadow-md transition-all duration-100"
          >
            <div className=" break-normal w-full">
              <div>
                <div className="text-xs text-left">
                  <h1 className="text-blue-600 text-center dark:text-blue-400 font-bold uppercase text-md">
                    {materials?.at(index).title}
                  </h1>
                  <div>
                      {materials?.at(index).content}
                  </div>
                </div>
                
              </div>
              <p className="flex text-left text-xs md:text-sm text-blue-700 dark:text-blue-400 font-bold leading-normal items-center">
                <svg
                  height="21"
                  viewBox="0 0 21 21"
                  width="21"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    transform="translate(6 6)"
                  >
                    <path d="m8.5 7.5v-7h-7"></path>
                    <path d="m8.5.5-8 8"></path>
                  </g>
                </svg>
                {/* News Site */}
              </p>
            </div>
          </div>
        </div>
        <div className="sticky bottom-0 lg:px-4 w-full mt-2 ">
          <div className="flex justify-center">
            <div className="bg-white rounded-xl mb-1 shadow-md drop-shadow-md flex w-full lg:w-1/2 p-2 justify-between h-12 lg:h-fit items-center">
              <Button
                className="border-none"
                id="prev"
                onClick={handlePage}
              >
                <BiLeftArrow onClick={e => prev.click()}/>
              </Button>
              {/* <button
                onClick="handleRotation"
                className="bg-blue-400 h-3 transition-all w-3 self-center duration-150 hover:scale-150 active:scale-125 ease-in-out rounded-md xl:p-1 shadow"
              ></button> */}
              <Button
                className="border-none"
                id="next"
                onClick={handlePage}
              >
                <BiRightArrow onClick={e => next.click()}/>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
}

export default Materials;
